function [accuracy] = ConvolutionalNN()
TRAIN_IMAGES = loadMNISTImages('train-images.idx3-ubyte');  
TRAIN_LABELS = loadMNISTLabels('train-labels.idx1-ubyte');  
TEST_IMAGES = loadMNISTImages('t10k-images.idx3-ubyte');  
TEST_LABELS = loadMNISTLabels('t10k-labels.idx1-ubyte');  


testSize = size(TEST_IMAGES);

trainData = TRAIN_IMAGES(:,:,1:54000);  
validationData = TRAIN_IMAGES(:,:,54001:end);
N = 54000;
V = 6000;
numValidation=V;
O = 10;

trainLabel = TRAIN_LABELS(1:54000); 
validationLabel = TRAIN_LABELS(54001:end);


w_mat=cell(2,1);
in_vec=cell(3,1);
d_e_vec=cell(3,1);
d_vec=cell(2);
I= eye(O); 

w_mat{1}= rand(15,144);
w_mat{2}= rand(10,15);

w=randn(5,5);

max_iter=100;
ita=0.125;
ita2=0.125;
BETA=0.01;

for iter=1:max_iter
    for n=1:N
        
        y1=sigmf(conv2(trainData(:,:,n),w,'valid'),[BETA,0]);
        
        %downsampling

         y = zeros((size(y1,1)/2)*(size(y1,2)/2),1);
         parfor c = 1:size(y,1)
             yr = floor((c - 1)/(size(y1,2)/2));
             yc = (c-1) - (size(y1,2)/2)*yr;
             row = 2*yr+1;
             col = 2*yc+1;
             ytemp = y1(row:row+1,col:col+1);
             y(c,1) = max(ytemp(:));
         end
        %         for c = 1:size(y,1)
%             for h = 1:size(y,2)
%                 %fprintf('%d %d %d %d\n',2*c + 1,2*c + 2, 2*h + 1,2*h + 2);
%                 row = 2*(c-1)+1;
%                 col = 2*(h-1)+1;
%                 ytemp = y1(row:row+1,col:col+1);
%                 y(c,h) = max(ytemp(:));
%             end
%         end
            
            
            
        %maxpoollayer = maxPooling2dLayer(2);       
        %y = maxpoollayer(y1);
        %converting to vector for input into fully connected layer
        %input=reshape(y',size(y,1)*size(y,2),1);
        input=y;
        in_vec{1}=input;
        %hidden layer
        in_vec{2}=sigmf(w_mat{1}*in_vec{1},[BETA,0]);
        %output layer
        in_vec{3}=softmax(w_mat{2}*in_vec{2});
        
        %backpropagation
        %output layer error
        d_e_vec{3}=-(I(:, trainLabel(n) + 1)-in_vec{3});
        
        d_vec{2}=d_e_vec{3};
        
        %hidden layer error
        d_e_vec{2}=w_mat{2}'*d_vec{2};
        %onemat = ones(size(in_vec{2}, 1), size(in_vec{2}, 2));
        d_vec{1}=d_e_vec{2}.*in_vec{2}.*(1-in_vec{2});
        
        d_e_vec{1}=w_mat{1}'*d_vec{1};           
        
        %now update!
        w_mat{1}=w_mat{1}-ita*d_vec{1}*in_vec{1}';
        w_mat{2}=w_mat{2}-ita*d_vec{2}*in_vec{2}';
        
        
        d_e_mat=reshape(d_e_vec{1},size(y,1),size(y,2));
        d_e_mat=d_e_mat';
        d_o_conv=y1.*(1-y1).*kron(d_e_mat,ones(2));
        gradient=rot180(conv2(trainData(:,:,n),rot180(d_o_conv),'valid'));
        w=w-ita2*gradient;
        
    end
    
    success=0;
     for i = 1:V  
         x=cell(3);
         y1=sigmf(conv2(validationData(:,:,i),w,'valid'),[BETA,0]);
         y = zeros((size(y1,1)/2)*(size(y1,2)/2),1);
         parfor c = 1:size(y,1)
             yr = floor((c - 1)/(size(y1,2)/2));
             yc = (c-1) - (size(y1,2)/2)*yr;
             row = 2*yr+1;
             col = 2*yc+1;
             ytemp = y1(row:row+1,col:col+1);
             y(c,1) = max(ytemp(:));
         end
%          y = zeros((size(y1,1)/2),(size(y1,2)/2));
%          for c = 1:size(y,1)
%             for h = 1:size(y,2)
%                 %fprintf('%d %d %d %d\n',2*c + 1,2*c + 2, 2*h + 1,2*h + 2);
%                 row = 2*(c-1)+1;
%                 col = 2*(h-1)+1;
%                 ytemp = y1(row:row+1,col:col+1);
%                 y(c,h) = max(ytemp(:));
%             end
%          end
         %maxpoollayer = maxPooling2dLayer(2);       
         %y = maxpoollayer(y1);
         input=y;%reshape(y',size(y,1)*size(y,2),1);
         
         x{1} = input;     
         x{2}=sigmf(w_mat{1}*x{1},[BETA,0]);
         x{3}=softmax(w_mat{2}*x{2});
         
         [~, m] = max(x{3});  
         if (m == validationLabel(i) + 1)  
           success = success + 1;  
         end  
     end 
     accuracy=(double(success)/double(numValidation));
     fprintf('accuracy = %f', accuracy*100.0);
     disp('%');
     if((success>=0.90*numValidation))         
         break;
     end
     
     if(iter==max_iter)
          fprintf('Maximum reached.\n');  
     end
    
end



 testSuccess = 0;  
 for i = 1:testSize(3)  
    x=cell(3);
    y1=sigmf(conv2(TEST_IMAGES(:,:,i),w,'valid'),[BETA,0]);
    %maxpoollayer = maxPooling2dLayer(2);       
%     %y = maxpoollayer(y1);
%     y = zeros((size(y1,1)/2),(size(y1,2)/2));
%     for c = 1:size(y,1)
%         for h = 1:size(y,2)
%             %fprintf('%d %d %d %d\n',2*c + 1,2*c + 2, 2*h + 1,2*h + 2);
%             row = 2*(c-1)+1;
%             col = 2*(h-1)+1;
%             ytemp = y1(row:row+1,col:col+1);
%             y(c,h) = max(ytemp(:));
%         end
%     end
         y = zeros((size(y1,1)/2)*(size(y1,2)/2),1);
         parfor c = 1:size(y,1)
             yr = floor((c - 1)/(size(y1,2)/2));
             yc = (c-1) - (size(y1,2)/2)*yr;
             row = 2*yr+1;
             col = 2*yc+1;
             ytemp = y1(row:row+1,col:col+1);
             y(c,1) = max(ytemp(:));
         end
    input=y;%reshape(y',size(y,1)*size(y,2),1);
   
   x{1} = input;     
   x{2}=sigmf(w_mat{1}*x{1},[BETA,0]);
   x{3}=softmax(w_mat{2}*x{2});
   
   [~, m] = max(x{3});    
   if (m == TEST_LABELS(i) + 1)  
     testSuccess = testSuccess + 1;  
   end  
 end
 accuracy=testSuccess/testSize(3);
 fprintf('test set accuracy = %f', accuracy*100.0);
 disp('%');