fid = fopen('trainData.txt', 'wt'); % Open for writing
for i=1:size(trainData,1)
   fprintf(fid, '%d ', trainData(i,:));
   fprintf(fid, '\n');
end
fclose(fid);
fid = fopen('trainLabel.txt', 'wt'); % Open for writing
for i=1:size(trainLabel,1)
   fprintf(fid, '%d ', trainLabel(i,:));
   fprintf(fid, '\n');
end
fclose(fid);
fid = fopen('testData.txt', 'wt'); % Open for writing
for i=1:size(testData,1)
   fprintf(fid, '%d ', testData(i,:));
   fprintf(fid, '\n');
end
fclose(fid);
fid = fopen('testLabel.txt', 'wt'); % Open for writing
for i=1:size(testLabel,1)
   fprintf(fid, '%d ', testLabel(i,:));
   fprintf(fid, '\n');
end
fclose(fid);