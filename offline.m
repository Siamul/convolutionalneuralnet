function [accuracy] = ANN(trainData, trainLabel, testData, testLabel, nodeNo, layerNo)
    imageTrain=trainData(1:10000,784);
    labelTrain=trainLabel(1:10000,1);
    imageTest=testData(1:1000,784);
    labelTest=testLabel(1:1000,1);

    trainSize = size(imageTrain);  
    testSize = size(imageTest);  
    totalNoTrainData = trainSize(1); 

    validpart=0.1; %10 percent of data for validation
    indices = randperm(totalNoTrainData);
    numTrain = int32((1-validpart)*totalNoTrainData); 
    numValidation=totalNoTrainData-numTrain;
    trainData = imageTrain(indices(1:numTrain),:);  
    validationData = imageTrain(indices(numTrain+1:end),:);
    trainLabel = labelTrain(indices(1:numTrain)); 
    validationLabel = labelTrain(indices(numTrain+1:end));
    weightmatrices = cell(layerNo,1);
    inputs = cell(layerNo+1, 1);
    del = cell(layerNo+1, 1);
    for c = 1:layerNo
        weightmatrices{c} = randn(nodeNo(c), nodeNo(c+1));
    end
    TOTAL_IN = trainSize(2);  
    TOTAL_OUT = 10;  
    MAX_ITERATION = 10000;   
    alpha = 0.01; % Scaling factor in sigmoid function  
    mu = 0.2; % Learning rate  
    
    for count = 1:MAX_ITERATION
        for i = 1:numTrain
            inputs{i} = [trainData(i,:),1];
            inputs{i} = transpose(inputs{i});
            %forward propagation
            for u = 1:layerNo-1
                inputs{u+1} = sigmoid(weightmatrices{u}*inputs{u});
            end
            inputs{layerNo+1} = softmax(weightmatrices{layerNo}*inputs{layerNo});
            %backward propagation
            for l = layerNo:-1:1
                
    
end